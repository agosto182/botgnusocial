import feedparser
import xml.etree.ElementTree as ET
from xml.dom import minidom
from gnusocial import statuses
from config import data as _config
import time

def saveLastStatus(last_url, last_title, dbName):
    feeds = ET.Element("feeds")
    feed = ET.SubElement(feeds, "feed")
    title = ET.SubElement(feed, "title")
    url = ET.SubElement(feed, "url")

    title.text = last_title
    url.text = last_url

    data = ET.tostring(feeds)
    db = open(dbName,"wb")

    db.write(data)

    db.close()

print("Iniciando..")
while True:
    for config in _config['bots']:
        parsed_feed = feedparser.parse(config['source']) # Obtenemos rss
        data = minidom.parse(config['dbName']) # Datos XML
        feeds = data.getElementsByTagName("feed")

        urls = feeds[0].getElementsByTagName("url")
        url = urls[0].firstChild.data # url del XML
        # print(parsed_feed.entries)
                   
        try:
            last_url = parsed_feed.entries[0].link # url mas nuevo
            last_title = parsed_feed.entries[0].title # titulo mas nuevo
        except Exception as err:
            print("Fallo la lectura del RSS")
            print(str(err))
            last_url = ""

        if(last_url!="" and str(url) != str(last_url)):
            saveLastStatus(last_url, last_title, config['dbName'])
            try:
                r = statuses.update(config['host'], data={'status': last_title, 'source':'AgostoBots'}, auth=(config['username'], config['password']))
                print("Nuevo status en "+config['username'])
                print(last_url)
                print('------')
            except Exception as err:
                print("Fallo la publicacion del status")

        else:
            print('Actualizado '+config['username'])
            print('Ultimo: '+str(url)+" - "+str(last_url))
    print('pausado..')
    time.sleep(_config['sleep'])
